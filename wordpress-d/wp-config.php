<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', '_rizm_jt9xkx5cty');

/** MySQL データベースのユーザー名 */
define('DB_USER', '_rizm_jt9xkx5cty');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'ra1uxiqknacjs');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql130.heteml.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A4da-{&Yxpo>q/E<(vQOEWktrZ3I=&1S9U..v=li7P1nd`3k-K>P92}sR][j|/[}');
define('SECURE_AUTH_KEY',  'hELSW<[Tf/|~I[$Tf[$g/BOfXjFzxa>U9d)Wt~+PMHszcvJ|)M7@sM0F<``$0OYz');
define('LOGGED_IN_KEY',    '-gL59VLM7_CZ+p1=[yH%B}KRYjVO=/E~tu`tFk`D8b6*Hx`dmhh)9-ZXv:($}KC.');
define('NONCE_KEY',        '_b5eC[!Ax8J`vi2YGafGl`7XK%anO7znZvI5(J{4Ij?^I.vsmR>Or<&[@g9jeyN^');
define('AUTH_SALT',        'jz?q~Qll@59c)vLuDd}:uEG%%Foi^]67-<oLgP<1K<TS!8DEcfSM$3R*}{rWSoUe');
define('SECURE_AUTH_SALT', '4}+bs{S^M(pN&.vhsE:w}f5a6m=ORm.M%[)wRa,8h0Lm#|KvgYi8_dZ@Gqj1SvDX');
define('LOGGED_IN_SALT',   ')LKZ(UXnK$QNA4}!S!9reY!%Fk61WAPeq3^wOva2y,G?%j@Xk?EEsFJ=,F*hjYBQ');
define('NONCE_SALT',       'RZC9K72~)}S.#njm7),pNT6Y3&`ddb/;@Q;fN*8VGRhI`G;]O`!6t7Un$tGgLTwf');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

add_filter('xmlrpc_enabled', '__return_false');

add_filter('xmlrpc_methods', function($methods) {
    unset($methods['pingback.ping']);
    unset($methods['pingback.extensions.getPingbacks']);
    return $methods;
});