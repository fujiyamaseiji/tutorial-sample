
jQuery(function($) {
  /* activate sidebar */
$('#sidebar').affix({
  offset: {
    top: 235,
    bottom: 235,
        top: 235

  }
});

jQuery(function($) {
/* smooth scrolling sections */
$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 100
        }, 500);
        return false;
      }
    }
});
});

// sidebarの高さとウィンドウの高さ長すぎる場合
// var $sidebar = $('#sidebar')
//     ,$w = $(window);
// $w.resize(function () { 
//   var pos = $w.height() < $sidebar.height()? 'absolute': 'fixed'; 
//   $sidebar.css({position: pos});
// });

// scrollspy と h2の関係topの位置
$('body').scrollspy({ target: '#sidebar', offset:110 });



jQuery(function($) {
$(document).ready(function(){
/* direct anchor access sections */
var indexPound = document.URL.lastIndexOf("#");
if(0 < indexPound && indexPound < document.URL.length-1 && $(document).width()>1200){
var AncRegexp = new RegExp("#(.+)$", "");
document.URL.match(AncRegexp);
var Anc = RegExp.$1;
$('html, body').animate({
scrollTop: ($('#'+Anc).offset().top()) - $(".navbar-fixed-top").height(),
}, {}
)
}
});
});


// sidebar scrollspy
  (function(window, $){
    $(function(){
      'use strict';
      // スクロール位置と連動させない場合は不要(scrollspy用)
      $(document.body).scrollspy({ target: '#sidebar' });
      // スクロール位置と連動させない場合は不要(scrollspy用)
      $(window).on('load', function() { $(document.body).scrollspy('refresh') });
    });
  })(window, jQuery);

   });





/*--------------------------------------------------------------------------*
 *  
 *  footerFixed.js
 *  
 *  MIT-style license. 
 *  
 *  2007 Kazuma Nishihata [to-R]
 *  http://blog.webcreativepark.net
 *  
 *--------------------------------------------------------------------------*/

new function(){
  
  var footerId = "footer";
  //メイン
  function footerFixed(){
    //ドキュメントの高さ
    var dh = document.getElementsByTagName("body")[0].clientHeight;
    //フッターのtopからの位置
    document.getElementById(footerId).style.top = "0px";
    var ft = document.getElementById(footerId).offsetTop;
    //フッターの高さ
    var fh = document.getElementById(footerId).offsetHeight;
    //ウィンドウの高さ
    if (window.innerHeight){
      var wh = window.innerHeight;
    }else if(document.documentElement && document.documentElement.clientHeight != 0){
      var wh = document.documentElement.clientHeight;
    }
    if(ft+fh<wh){
      document.getElementById(footerId).style.position = "relative";
      document.getElementById(footerId).style.top = (wh-fh-ft-0)+"px";
    }
  }
  
  //文字サイズ
  function checkFontSize(func){
  
    //判定要素の追加 
    var e = document.createElement("div");
    var s = document.createTextNode("S");
    e.appendChild(s);
    e.style.visibility="hidden"
    e.style.position="absolute"
    e.style.top="0"
    document.body.appendChild(e);
    var defHeight = e.offsetHeight;
    
    //判定関数
    function checkBoxSize(){
      if(defHeight != e.offsetHeight){
        func();
        defHeight= e.offsetHeight;
      }
    }
    setInterval(checkBoxSize,1000)
  }
  
  //イベントリスナー
  function addEvent(elm,listener,fn){
    try{
      elm.addEventListener(listener,fn,false);
    }catch(e){
      elm.attachEvent("on"+listener,fn);
    }
  }

  addEvent(window,"load",footerFixed);
  addEvent(window,"load",function(){
    checkFontSize(footerFixed);
  });
  addEvent(window,"resize",footerFixed);
  
}

// backtoppage
jQuery(function($) {
$(document).ready(function(){

  // hide #back-top first
  $("#back-top").hide();
  
  // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });

    // scroll body to 0px on click
    $('#back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });

});

});


// serchform focus
$(document).ready(function(){
    $("#searchform").focusin(function(){
        $(this).css("color", "#333");
    });
    $("#searchform").focusout(function(){
        $(this).css("color", "#fff");
    });
});

$(document).ready(function(){
    $(".placecolor").focusin(function(){
        $(this).css("color", "#000");
    });
    $(".placecolor").focusout(function(){
        $(this).css("color", "#fff");
    });
});




// テスト中
  // jQuery(function() {
  //   var nav = jQuery('#sub-nav');　//固定したいDivのIDを入力
 
  //   // メニューのtop座標を取得する
  //   var offsetTop = nav.offset().top;
 
  //   var floatMenu = function() {
  //     // スクロール位置がメニューのtop座標を超えたら固定にする
  //     if (jQuery(window).scrollTop() > offsetTop) {
  //       nav.addClass('fixed');
  //     } else {
  //       nav.removeClass('fixed');
  //     }
  //   }
  //   jQuery(window).scroll(floatMenu);
  //   jQuery('body').bind('touchmove', floatMenu);
  // });

