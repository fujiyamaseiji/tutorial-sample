<!DOCTYPE html>
<html lang="ja"><head>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0">  -->
<title><?php wp_title('|', true, 'right'); bloginfo('name');?></title> 
<link href="<?php echo get_template_directory_uri(); ?>/boot/css/bootstrap.min.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> 
<?php 
$cats = get_the_category();
$cat = $cats[0];
if($cat->parent){
$parent = get_category($cat->parent);
$menucat = $parent->slug;
}else{
$menulug = $cat->slug;
}
?>
<?php if ( is_home() || is_front_page() ) : ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php elseif ( is_search()|| is_archive()|| is_tag() ): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php else : ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $menucat;?>.css" />
<?php endif; ?>


<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
</head>
<body  data-spy="scroll" data-offset="62" data-target=".scroll-spy-sample" <?php body_class(); ?> >

<div id="header" class="clearfix" >
<!-- Navigation -->
<div id="toggle"><a href="#"></a></div><!--マルチでバイス用に設定-->
<!-- ヘッダー固定 -->
<nav id="navbar-scrollspy" class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="header-wrap"><span class="header-logo"><a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" title="DOCS"></a></span>


<!-- Navigation swicher -->
<?php if ( is_home() || is_front_page() ) : ?>
<?php wp_nav_menu( array ( 'theme_location' => 'header-navi' ) ); ?>
<?php elseif ( is_search()|| is_archive()|| is_tag() ): ?>
<?php wp_nav_menu( array ( 'theme_location' => 'header-navi' ) ); ?>
<?php else : ?>
<?php wp_nav_menu( array('menu' => ''.$menucat.'', 'container' => '')); ?>
<?php endif; ?>

<!-- /Navigation -->
<div id="serch-wrap" ><?php get_search_form(); ?></div>
</div>
</nav>
</div><!-- header -->
<div id="topline-wrap">
<div id="masthead">  
<div class="container">
<div class="row ">
<div class="col-xs-8">
<div class="breadcrumbs">
<?php if(function_exists('bcn_display')){
bcn_display();
}?>


</div><!--/breadcrumbs-->
</div><!--/col-xs-8-->
<div class="col-xs-4">
<div class="well well-sm"> 
<div class="row">
<div class="col-xs-12">
Tag : <?php
$posttags = get_the_tags();
if ( $posttags ) {
foreach ( $posttags as $tag ) {
echo '<a href="'.get_tag_link( $tag->term_id).'" >' ;
echo $tag->name . ' &nbsp; '; 
echo '</a>';
}
}
else{

	?>

	<style type="text/css">
.well {display:none;
   
}
</style>

	<?php
echo '<span >unset</span>';
}
?>
</div>
</div>
</div>
</div><!--/col-xs-4-->
</div> <!--/row-->
</div><!--/container-->
</div><!--/masthead-->
</div> 
<div id="container">

