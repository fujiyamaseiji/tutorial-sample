<?php get_header(); ?>
<div class="row">
<div class="col-md-9">
<?php 
if (have_posts()) : // WordPress ループ
while (have_posts()) : the_post(); // 繰り返し処理開始 ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h1>リスト</h1>

<div class="list-group">
<h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
<p class="post-meta">
<span class="category"><?php the_category(', ') ?></span>

</p>
<p><?php echo mb_substr(get_the_excerpt(), 0, 240); ?></p>
</div>

</div>



<?php 
endwhile; // 繰り返し処理終了		
else : // ここから記事が見つからなかった場合の処理 ?>
<div class="post">
<h2>お探しのURLはございません</h2>
<p>ネビゲーション、又は検索フォームからもう一度お探しください。</p>
</div>
<?php
endif;
?>

<!-- pager -->
<?php
if ( $wp_query -> max_num_pages > 1 ) : ?>
<div class="navigation">
<div class="alignleft"><?php next_posts_link('&laquo; PREV'); ?></div>
<div class="alignright"><?php previous_posts_link('NEXT &raquo;'); ?></div>
</div>
<?php 
endif;
?>
<!-- /pager	 -->
</div><!-- /col left -->

<div class="col-md-3"><!-- col right -->
<div id="sidebar">
<?php dynamic_sidebar('sidebar 1'); ?>
</div>
</div>
</div><!-- /row -->
<?php get_footer(); ?>