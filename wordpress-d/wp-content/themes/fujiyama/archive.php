<?php
/**
* The template for displaying Archive pages.
*/

get_header('single'); ?>
<div class="row">
<div class="col-xs-9">
<h1>Archive for <?php the_time('F Y'); ?></h1>
<?php 
if (have_posts()) : // WordPress ループ
while (have_posts()) : the_post(); // 繰り返し処理開始 ?>

<div class="list-group">
<a href="<?php the_permalink(); ?>" class="list-group-item"><?php the_title(); ?>
<p><?php echo mb_substr(get_the_excerpt(), 0, 240); ?></p>
<p><?php the_time('Y/m/d'); ?></p></a>
</div>
<?php 
endwhile; 	
else : // ここから記事が見つからなかった場合の処理 ?>
<div class="post">
<h2>お探しのURLはございません</h2>
<p>ネビゲーション、又は検索フォームからもう一度お探しください。</p>
</div>
<?php
endif;
?><!-- pager -->
<?php
if ( $wp_query -> max_num_pages > 1 ) : ?>
<div class="navigation">
<div class="alignleft"><?php next_posts_link('&laquo; PREV'); ?></div>
<div class="alignright"><?php previous_posts_link('NEXT &raquo;'); ?></div>
</div>
<?php 
endif;
?>
</div><!-- /col left -->
<div class="col-xs-3"><!-- col right -->
<div id="sidebar" class="panel panel-default">
<?php dynamic_sidebar('sidebar 1'); ?>
</div>
</div>
</div><!-- /row -->
<?php get_footer(); ?>