<?php
/**
 * The template for displaying 404 pages (Not Found).

 */

get_header("2"); ?>
<div class="row">
<div class="col-xs-9">
<div id="post-0" class="post error404 not-found">
<h1 class="entry-title"><?php _e( 'Not Found', '' ); ?></h1>
<div class="notfound">404</div>
</div><!-- #post-0 -->
</div><!-- /col left -->
<div class="col-xs-3"><!-- col right -->
<div id="sidebar" class="panel panel-default">
<?php dynamic_sidebar('sidebar 1'); ?>
</div>
</div>
</div><!-- /row -->
<?php get_footer(); ?>