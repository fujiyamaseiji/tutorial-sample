<?php
/*
Template Name: Search
*/
?>
<?php get_header(); ?>

<div class="row">
<div class="col-xs-9">
<div class="nwrapper">
<h1>検索結果</h1>
<?php $allsearch =& new WP_Query("s=$s&posts_per_page=-1");
$key = wp_specialchars($s, 1);
$count = $allsearch->post_count;
if($count!=0){
// 検索結果を表示:該当記事あり
    echo '<p>“<strong>'.$key.'</strong>”で検索した結果、<strong>'.$count.'</strong>件の記事が見つかりました</p>';
} 
else {
// 検索結果を表示:該当記事なし
    echo '<p>“<strong>'.$key.'</strong>”で検索した結果、関連する記事は見つかりませんでした</p>';
}
?>
<?php if($s){ ?>検索キーワード：<?php echo $s; ?><br><?php } ?>
<?php
if (is_array($catnum)) { ?>カテゴリ：<?php
foreach($catnum as $val){
if ($val === end($catnum)) {
echo get_cat_name($val);
    }else{
echo get_cat_name($val).", ";
}
}
}
 ?>

<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
<div>カテゴリにて検索</div>
<select name="catnum">
<option value="" selected>選択</option>
<?php
$categories = get_categories("parent=0");
foreach($categories as $category) :
?>
<option value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>
<?php endforeach; ?>
</select>
<label for="s" class="assistive-text">検索</label>
<input type="text" name="s" id="s" placeholder="検索" />    
<input type="submit" value="検索" />
</form>

 <hr>
<?php
query_posts( array(
    'tax_query' => $taxquerysp,
    's' => $s,
    )
);
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="list-group">
<a href="<?php the_permalink(); ?>" class="list-group-item"><?php the_title(); ?>
<p><?php echo mb_substr(get_the_excerpt(), 0, 240); ?></p>
</a>
</div>
<?php endwhile; else : ?>
<div>該当なし</div>
<?php endif;
wp_reset_query(); ?>
</div><!-- /nwrapper -->
</div><!-- /col left -->
<div class="col-xs-3"><!-- col right -->
<div id="sidebar" class="panel panel-default">
<?php dynamic_sidebar('sidebar 1'); ?>
</div>
</div>
</div><!-- /row -->
<?php get_footer(); ?>

