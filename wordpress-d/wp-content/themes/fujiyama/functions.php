<?php

/*------------------------------------
 * メインコンテンツの幅を指定
 ------------------------------------ */

if ( ! isset( $content_width ) ) $content_width = 980;


/*------------------------------------
 * カスタムメニューを有効化
 ------------------------------------ */

add_theme_support( 'menus' );

/*------------------------------------
 * 管理ツールバー非表示
 ------------------------------------*/
add_filter( 'show_admin_bar', '__return_false' );
function my_cloud($echo = false) {
    if (function_exists('wp_tag_cloud'))
        return wp_tag_cloud();
}

/*------------------------------------
 * カスタムメニューの「場所」を設定
 ------------------------------------ */

register_nav_menu( 'header-navi', 'ヘッダーのナビゲーション' );


/*------------------------------------
 * サイドバーウィジットを有効化
 ------------------------------------ */

register_sidebar( array(
  'name' => 'サイドバーウィジット-1',
  'id' => 'sidebar-1',
  'description' => 'サイドバーのウィジットエリアです。',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
) );

/*------------------------------------
 * ヘッダの余計なタグを無効化
 ------------------------------------ */

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_generator' ); 


// サーチフォームはcssでnoneに設定してあるが機能だけは追加しておく
 function post_id_search_where( $where, $obj )
{
  global $wpdb;
  if( $obj->is_search ) {
    $where = preg_replace(
      "/\(\s*$wpdb->posts\.post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
      "($wpdb->posts.post_title LIKE $1) OR ($wpdb->posts.ID LIKE $1)", $where );
  }
  return $where;
}
add_filter('posts_where', 'post_id_search_where', 10, 2 );



// ログイン状態を保存するにチェック

function login_rememberme_check() { ?>
 <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
 <script>
 $(document).ready(function(){
 $('#rememberme').prop('checked', true);
 });
 </script>
<?php }
add_action( 'login_enqueue_scripts', 'login_rememberme_check' );



/*------------------------------------
* WordPress本体のバージョンアップ通知　更新　バージョンアップ表示をOFF
 ------------------------------------ */
add_filter('pre_site_transient_update_core', '__return_zero');
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');
// プラグイン通知
add_filter('site_option__site_transient_update_plugins', '__return_zero');




/*------------------------------------
* 管理バーのヘルプメニューを非表示にする
 ------------------------------------ */
function my_admin_head(){
 echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
 }
add_action('admin_head', 'my_admin_head');

/*------------------------------------
* フッターWordPressリンクを非表示に
 ------------------------------------ */
function custom_admin_footer() {
 echo '';
 }
add_filter('admin_footer_text', 'custom_admin_footer');



/*------------------------------------
  アーカイブページで現在のカテゴリー・タグ・タームを取得する
 ------------------------------------ */
function get_current_term(){

  $id;
  $tax_slug;

  if(is_category()){
    $tax_slug = "category";
    $id = get_query_var('cat'); 
  }else if(is_tag()){
    $tax_slug = "post_tag";
    $id = get_query_var('tag_id');  
  }else if(is_tax()){
    $tax_slug = get_query_var('taxonomy');  
    $term_slug = get_query_var('term'); 
    $term = get_term_by("slug",$term_slug,$tax_slug);
    $id = $term->term_id;
  }

  return get_term($id,$tax_slug);
}

/*------------------------------------
 カスタムmenu認識
 親カテゴリーでheaderの切り替え
 ------------------------------------ */

function post_is_in_descendant_category( $cats, $_post = null )
{
          foreach ( (array) $cats as $cat ) {
                    // get_term_children() accepts integer ID only
                    $descendants = get_term_children( (int) $cat, 'category');
                    if ( $descendants && in_category( $descendants, $_post ) )
                              return true;
          }
          return false;
}

/*------------------------------------
検索ページ　カテゴリー絞り込み
 ------------------------------------ */

function add_category_to_page() {
    register_taxonomy_for_object_type('category', 'page');
}
add_action('init', 'add_category_to_page');

function show_categorized_pages_in_archive( $query ) {
    if ( $query->is_category && $query->is_main_query() ) {
        $query->set('post_type', array( 'post', 'page', 'nav_menu_item'));
    }
}
add_action( 'pre_get_posts', 'show_categorized_pages_in_archive' );



/*------------------------------------
サイドバー　特定カテゴリー除外
 ------------------------------------ */
// function cat_limited_wp_get_archives( $where ) {
//   global $wpdb;
//   $cat_string = 1;
//   $where .= " AND $wpdb->posts.ID NOT IN ( SELECT tr.object_id FROM $wpdb->term_relationships AS tr INNER JOIN $wpdb->term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy = 'category' AND tt.term_id IN ($cat_string) )";
//   return $where;
// }
// add_filter( 'getarchives_where', 'cat_limited_wp_get_archives' );
