

<?php
get_header(); 
?>


<div class="row">
<div class="col-xs-9">
<?php 
if (have_posts()) : // WordPress ループ
while (have_posts()) : the_post(); // 繰り返し処理開始 ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h1><?php echo get_the_title(); ?></h1>
<div class="contentslinset">
<?php the_content(); ?>
</div>
</div>
<?php 
endwhile; // 繰り返し処理終了		
else : // ここから記事が見つからなかった場合の処理 ?>
<div class="post">
<h2>お探しのURLはございません</h2>
<p>ネビゲーション、又は検索フォームからもう一度お探しください。</p>
</div>
<?php
endif;
?>
</div><!-- /col left -->
<div class="col-xs-3"><!-- col right -->
<div id="sidebar" class="panel panel-default">
<nav>
<?php
if( in_category('all_questions') ) {get_sidebar('all_questions');}//1

else {
get_sidebar();
}
?>
</nav>
</div> 
</div>
</div><!-- contents-wrap -->
<?php get_footer(); ?>